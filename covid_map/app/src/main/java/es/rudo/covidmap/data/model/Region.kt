package es.rudo.covidmap.data.model

class Region(
    val nombreLugar: String,
    val long: Double,
    val lat: Double,
    val codigoIsoLugar: String,
    val data: Data,
) {
}
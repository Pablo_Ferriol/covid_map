package es.rudo.covidmap.helpers

import android.content.Context
import android.content.SharedPreferences

class AppPreferences(val context: Context) {

    private val PREF_FILE = "MyPreferences"
    private val MAX_DATE = "MaxDate"
    private val MIN_DATE = "MinDate"
    private val MIN_YEAR = "MinYear"
    private val CCAA_CODE = "CcaaCode"
    private val LAST_DATE = "Last_Date"

    private fun getSharedPreferences(): SharedPreferences? {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE)
    }

    fun getMaxDate(): String? {
        return getSharedPreferences()?.getString(MAX_DATE, null)
    }

    fun setMaxDate(MaxDate: String?) {
        this.getSharedPreferences()?.edit()?.putString(MAX_DATE, MaxDate)?.apply()
    }

    fun getMinDate(): String? {
        return getSharedPreferences()?.getString(MIN_DATE, null)
    }

    fun setMinDate(MinDate: String?) {
        this.getSharedPreferences()?.edit()?.putString(MIN_DATE, MinDate)?.apply()
    }

    fun getMinYear(): String? {
        return getSharedPreferences()?.getString(MIN_YEAR, null)
    }

    fun setMinYear(MinYear: String?) {
        this.getSharedPreferences()?.edit()?.putString(MIN_YEAR, MinYear)?.apply()
    }

    fun getCcaaCode(): String? {
        return getSharedPreferences()?.getString(CCAA_CODE, null)
    }

    fun setCcaaCode(CcaaCode: String?) {
        this.getSharedPreferences()?.edit()?.putString(CCAA_CODE, CcaaCode)?.apply()
    }

    fun getLastDate(): String? {
        return getSharedPreferences()?.getString(LAST_DATE, null)
    }

    fun setLastDate(lastDate: String?) {
        this.getSharedPreferences()?.edit()?.putString(LAST_DATE, lastDate)?.apply()
    }

    fun cleanLastDate() {
        this.getSharedPreferences()?.edit()?.remove(LAST_DATE)?.apply()
    }

}

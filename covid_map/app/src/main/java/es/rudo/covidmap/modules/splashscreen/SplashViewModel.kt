package es.rudo.covidmap.modules.splashscreen

import android.util.Log
import androidx.lifecycle.ViewModel
import es.rudo.covidmap.App
import es.rudo.covidmap.api.RetrofitClient
import es.rudo.covidmap.data.model.Header
import es.rudo.covidmap.data.model.Info
import es.rudo.covidmap.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class SplashViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()

    init {
        getMaxMinDate()
    }

    fun getMaxMinDate() {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                val code = "ES-VC"
                retrofitClient.getMaxMinDate(code)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseHeader: Header = response.body() as Header
                            val info: Info = responseHeader.trace.info
                            App.preferences.setMaxDate(info.fechaFin)
                            App.preferences.setMinDate(info.fechaInicio)
                            App.preferences.setMinYear(info.fechaInicio.substring(0, 4))
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}
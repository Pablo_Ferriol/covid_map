package es.rudo.covidmap.helpers

object Constants {
    //Server code
    const val SERVER_SUCCESS_CODE = 200

    //Formats
    const val SERVER_DATE_PATTERN = "yyyy-MM-dd"

    //CHECK
    const val MIN_CHECK = "00"
}
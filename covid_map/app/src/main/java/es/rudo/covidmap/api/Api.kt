package es.rudo.covidmap.api

import es.rudo.covidmap.data.model.Header
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    //GET REGIONS
    @GET("/api/v1/es/ccaa?ultimodia=true")
    suspend fun getRegion(): Response<Header>

    //GET LAST COVID DETAILS
    @GET("/api/v1/es/ccaa/")
    suspend fun getLastCovidDetails(
        @Query("ultimodia") lastday: Boolean,
        @Query("codigo") cod: String?
    ): Response<Header>

    //GET FILTER
    @GET("/api/v1/es/ccaa/")
    suspend fun getFilter(
        @Query("fecha") fecha: String?,
        @Query("codigo") cod: String?
    ): Response<Header>

    //GET MAX MIN DATE OF A CCAA
    @GET("/api/v1/es/ccaa/")
    suspend fun getMaxMinDate(
        @Query("codigo") cod: String?
    ): Response<Header>
}
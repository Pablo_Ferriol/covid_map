package es.rudo.covidmap

import android.app.Application
import android.content.Context
import es.rudo.covidmap.helpers.AppPreferences

class App : Application() {

    companion object {
        lateinit var preferences: AppPreferences
        lateinit var instance: App private set
        lateinit var context: Context
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        preferences = AppPreferences(applicationContext)
        context = this
    }
}
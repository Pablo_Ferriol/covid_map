package es.rudo.covidmap.data.model

class Data(
    val casosConfirmados: Int,
    val casosUci: Int,
    val casosFallecidos: Int,
    val casosHospitalizados: Int,
    val casosRecuperados: Int,
    val casosConfirmadosDiario: Int,
    val casosUciDiario: Int,
    val casosFallecidosDiario: Int,
    val casosHospitalizadosDiario: Int,
    val casosRecuperadosDiario: Int
) {
}
package es.rudo.covidmap.modules.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import es.rudo.covidmap.R
import es.rudo.covidmap.databinding.ActivitySplashBinding
import es.rudo.covidmap.modules.maps.MapsActivity

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    private lateinit var viewModel: SplashViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        viewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
        binding.lifecycleOwner = this
        splash()
    }

    private fun splash() {
        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(this, MapsActivity::class.java))
            finish()
        }, 2000.toLong())
    }
}
package es.rudo.covidmap.modules.covdetails

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.covidmap.App
import es.rudo.covidmap.api.RetrofitClient
import es.rudo.covidmap.data.model.Data
import es.rudo.covidmap.data.model.Header
import es.rudo.covidmap.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class CovidDetailsViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()

    //cases data
    val casosConfirmados = MutableLiveData<String>()
    val casosUci = MutableLiveData<String>()
    val casosFallecidos = MutableLiveData<String>()
    val casosHospitalizados = MutableLiveData<String>()
    val casosRecuperados = MutableLiveData<String>()
    val casosConfirmadosDiario = MutableLiveData<String>()
    val casosUciDiario = MutableLiveData<String>()
    val casosFallecidosDiario = MutableLiveData<String>()
    val casosHospitalizadosDiario = MutableLiveData<String>()
    val casosRecuperadosDiario = MutableLiveData<String>()

    //ccaa data
    val ccaaName = MutableLiveData<String>()
    val ccaaNameDate = MutableLiveData<String>()

    //date
    val dateYear = MutableLiveData<String>()
    val dateMonth = MutableLiveData<String>()
    val dateDay = MutableLiveData<String>()

    //Errors
    val serverError = MutableLiveData<Boolean>()
    val responseEmpty = MutableLiveData<Boolean>()

    //state
    val afterCall = MutableLiveData<Boolean>()

    fun getLastCovidDetails() {

        val date: String? =
            if (App.preferences.getLastDate().equals("Last_Date") || App.preferences.getLastDate()
                    .isNullOrEmpty()
            ) {
                App.preferences.getMaxDate()
            } else App.preferences.getLastDate()

        val code = App.preferences.getCcaaCode()
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getFilter(date, code)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            if (response.body() != null) {
                                val responseHeader: Header = response.body() as Header
                                val data: Data = responseHeader.timeline[0].regiones[0].data
                                setData(responseHeader, data)
                                ccaaNameDate.value = date
                                afterCall.value = true
                            } else {
                                responseEmpty.value = true
                            }
                        } else {
                            serverError.value = true
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

    fun getFilter() {
        val date = "${dateYear.value}-${dateMonth.value}-${dateDay.value}"
        val code = App.preferences.getCcaaCode()
        App.preferences.setLastDate(date)
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getFilter(date, code)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            if (response.body() != null) {
                                val responseHeader: Header = response.body() as Header
                                val data: Data = responseHeader.timeline[0].regiones[0].data
                                setData(responseHeader, data)
                                afterCall.value = true
                                ccaaNameDate.value = date
                            } else {
                                responseEmpty.value = true
                            }
                        } else {
                            serverError.value = true
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

    private fun setData(
        responseHeader: Header,
        data: Data
    ) {
        ccaaName.value = responseHeader.timeline[0].regiones[0].nombreLugar
        casosConfirmados.value = data.casosConfirmados.toString()
        casosUci.value = data.casosUci.toString()
        casosFallecidos.value = data.casosFallecidos.toString()
        casosHospitalizados.value = data.casosHospitalizados.toString()
        casosRecuperados.value = data.casosRecuperados.toString()
        casosConfirmadosDiario.value = data.casosConfirmadosDiario.toString()
        casosUciDiario.value = data.casosUciDiario.toString()
        casosFallecidosDiario.value = data.casosFallecidosDiario.toString()
        casosHospitalizadosDiario.value = data.casosHospitalizadosDiario.toString()
        casosRecuperadosDiario.value = data.casosRecuperadosDiario.toString()
    }
}

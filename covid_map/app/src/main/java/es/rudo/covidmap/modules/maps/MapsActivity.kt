package es.rudo.covidmap.modules.maps

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import es.rudo.covidmap.App
import es.rudo.covidmap.R
import es.rudo.covidmap.data.model.Region
import es.rudo.covidmap.databinding.ActivityMapsBinding
import es.rudo.covidmap.modules.covdetails.CovidDetailsActivity

class MapsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMapsBinding
    private lateinit var viewModel: MapsViewModel
    private var regionList: ArrayList<Region> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        viewModel = ViewModelProvider(this).get(MapsViewModel::class.java)
        binding.lifecycleOwner = this
        viewModel.getRegion()/*{ *//*setMap()*//* }*/
        initObserver()
    }
    /*TODO,
       -> Preguntar si es correcto el uso de funcion de orden superior. Ejemplo con getRegion.
       -> Preguntar antes de acabar las practicas mas información sobre callback y realizar funciones sync.
     */

    private fun initObserver() {
        viewModel.serverError.observe(this, Observer {
            if (it) Toast.makeText(this, "Ups, ha habído un problema", Toast.LENGTH_SHORT).show()
        })
        viewModel.setMap.observe(this, Observer {
            if (it) setMap()
        })
    }

    private fun setMap() {
        val supportMapFragment: SupportMapFragment? =
            supportFragmentManager.findFragmentById(R.id.frame_maps) as SupportMapFragment?
        viewModel.regionList.value?.let { regionList = it }
        confMaps(supportMapFragment)
    }

    private fun confMaps(supportMapFragment: SupportMapFragment?) {
        supportMapFragment?.getMapAsync { googleMap ->
            val spain = LatLng(40.463667, -3.74922)
            setMarkers(googleMap)
            markerListener(googleMap)
            Handler(Looper.getMainLooper()).postDelayed({
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(spain, 4.5f))
            }, 1000.toLong())
        }
    }

    private fun setMarkers(googleMap: GoogleMap) {
        regionList.let {
            for (item: Region in it) {
                val marker = MarkerOptions()
                marker.position(LatLng(item.lat, item.long))
                marker.title(item.codigoIsoLugar)
                googleMap.addMarker(marker)
            }
        }
    }

    private fun markerListener(googleMap: GoogleMap) {
        googleMap.setOnMarkerClickListener { marker ->
            val latlong = LatLng(marker.position.latitude, marker.position.longitude)
            App.preferences.setCcaaCode(marker.title)
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 6f))
            Handler(Looper.getMainLooper()).postDelayed({ //delay to see animation
                goToDetails()
            }, 1000.toLong())
            true
        }
    }

    fun goToDetails() {
        val intent = Intent(this, CovidDetailsActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        App.preferences.cleanLastDate()
    }
}



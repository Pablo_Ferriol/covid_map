package es.rudo.covidmap.modules.covdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import es.rudo.covidmap.App
import es.rudo.covidmap.R
import es.rudo.covidmap.databinding.ActivityCovidDetailsBinding
import es.rudo.covidmap.helpers.DatePikerFragment
import kotlinx.android.synthetic.main.general_popup.view.*


class CovidDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCovidDetailsBinding
    private lateinit var viewModel: CovidDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_covid_details)
        viewModel = ViewModelProvider(this).get(CovidDetailsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        firstProgressBar()
        setImage()
        setTittle()
        initObserver()
        getDetailsData()
        setActionBar()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.findItem(R.id.action_icon_help).setIcon(R.drawable.ic_baseline_help_24)
        menu.findItem(R.id.action_icon_calendar).setIcon(R.drawable.ic_baseline_calendar_today_24)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_icon_calendar -> datePickerDialog()
            R.id.action_icon_help -> popUpHelp()
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setActionBar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initObserver() {
        viewModel.serverError.observe(this, Observer {
            if (it) {
                Toast.makeText(this, getString(R.string.error), Toast.LENGTH_SHORT).show()
                afterCall()
            }
        })

        viewModel.responseEmpty.observe(this, Observer {
            if (it) {
                afterCallEmptyResponse()
            } else afterCall()
        })
        //style after call
        viewModel.afterCall.observe(this, Observer {
            if (it) {
                setWarningIcon()
                afterCall()
            }
        })
    }

    private fun getDetailsData() {
        binding.nestedContent.visibility = View.GONE
        binding.contentProgressBar.visibility = View.VISIBLE
        viewModel.getLastCovidDetails()
    }

    private fun setWarningIcon() {
        viewModel.casosConfirmadosDiario.value?.toIntOrNull()?.let {
            if (it >= 100) {
                binding.imageTittleWarning.visibility = View.VISIBLE
            }
            if (it < 100) {
                binding.imageTittleWarning.visibility = View.GONE
            }
        }
    }

    private fun afterCall() {
        //visible after open
        binding.toolbar.visibility = View.VISIBLE
        binding.imageCcaa.visibility = View.VISIBLE
        //setProgressBar
        binding.nestedContent.visibility = View.VISIBLE
        binding.contentProgressBar.visibility = View.GONE
    }

    private fun afterCallEmptyResponse() {
        val date = App.preferences.getLastDate()
        /*     "${viewModel.dateYear.value}-${viewModel.dateMonth.value}-${viewModel.dateDay.value}"*/
        //visible after open
        binding.toolbar.visibility = View.VISIBLE
        binding.imageCcaa.visibility = View.VISIBLE
        //setProgressBar
        binding.editTextNoResult.visibility = View.VISIBLE
        binding.contentProgressBar.visibility = View.GONE
        //SetText
        if (date.isNullOrEmpty()) App.preferences.getMaxDate().toString()
        binding.editTextNoResultDate.setText("${getString(R.string.response_empty_text_date)} $date")
    }

    private fun popUpHelp() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.general_popup, null)
        val builder = AlertDialog.Builder(this)
            .setView(dialogView)
        builder.setCancelable(false)
        val alertDialog = builder.show()
        //set popup style
        dialogView.popup_text_max_date.text =
            getString(R.string.popup_help_text_max) + App.preferences.getMaxDate()
        dialogView.popup_text_min_date.text =
            getString(R.string.popup_help_text_min) + App.preferences.getMinDate()
        //set click
        dialogView.popup_button_accept.setOnClickListener {
            alertDialog.dismiss()
        }
    }

    private fun datePickerDialog() {
        val datePicker = DatePikerFragment { day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    fun onDateSelected(day: Int, month: Int, year: Int) {
        viewModel.dateYear.value = year.toString()
        val selectedMonth = month + 1

        if (selectedMonth in 0..11) {
            viewModel.dateMonth.value = "0$selectedMonth"
        } else viewModel.dateMonth.value = selectedMonth.toString()
        if (day in 0..11) {
            viewModel.dateDay.value = "0$day"
        } else viewModel.dateDay.value = day.toString()

        binding.editTextNoResult.visibility = View.GONE
        binding.nestedContent.visibility = View.GONE
        binding.contentProgressBar.visibility = View.VISIBLE
        viewModel.getFilter()
    }

    private fun firstProgressBar() {
        binding.toolbar.visibility = View.GONE
        binding.nestedContent.visibility = View.GONE
        binding.imageCcaa.visibility = View.GONE
        binding.contentProgressBar.visibility = View.VISIBLE
    }

    fun setImage() {
        when (App.preferences.getCcaaCode()) {
            "ES-AN" -> Picasso.get().load(R.drawable.andalucia).into(binding.imageCcaa)
            "ES-AR" -> Picasso.get().load(R.drawable.aragon).into(binding.imageCcaa)
            "ES-AS" -> Picasso.get().load(R.drawable.asturias).into(binding.imageCcaa)
            "ES-IB" -> Picasso.get().load(R.drawable.baleares).into(binding.imageCcaa)
            "ES-CN" -> Picasso.get().load(R.drawable.canarias).into(binding.imageCcaa)
            "ES-CB" -> Picasso.get().load(R.drawable.cantabria).into(binding.imageCcaa)
            "ES-CM" -> Picasso.get().load(R.drawable.castilla).into(binding.imageCcaa)
            "ES-CL" -> Picasso.get().load(R.drawable.leon).into(binding.imageCcaa)
            "ES-CT" -> Picasso.get().load(R.drawable.catalu_a).into(binding.imageCcaa)
            "ES-CE" -> Picasso.get().load(R.drawable.ceuta).into(binding.imageCcaa)
            "ES-VC" -> Picasso.get().load(R.drawable.valencia).into(binding.imageCcaa)
            "ES-EX" -> Picasso.get().load(R.drawable.extremadura).into(binding.imageCcaa)
            "ES-GA" -> Picasso.get().load(R.drawable.galicia).into(binding.imageCcaa)
            "ES-MD" -> Picasso.get().load(R.drawable.madrid).into(binding.imageCcaa)
            "ES-ML" -> Picasso.get().load(R.drawable.melilla).into(binding.imageCcaa)
            "ES-MC" -> Picasso.get().load(R.drawable.murcia).into(binding.imageCcaa)
            "ES-NC" -> Picasso.get().load(R.drawable.navarra).into(binding.imageCcaa)
            "ES-PV" -> Picasso.get().load(R.drawable.paisvasco).into(binding.imageCcaa)
            "ES-RI" -> Picasso.get().load(R.drawable.rioja).into(binding.imageCcaa)
        }
    }

    fun setTittle() {
        when (App.preferences.getCcaaCode()) {
            "ES-AN" -> viewModel.ccaaName.value = "Andalucía"
            "ES-AR" -> viewModel.ccaaName.value = "Aragón"
            "ES-AS" -> viewModel.ccaaName.value = "Asturias"
            "ES-IB" -> viewModel.ccaaName.value = "Baleares"
            "ES-CN" -> viewModel.ccaaName.value = "Canarias"
            "ES-CB" -> viewModel.ccaaName.value = "Cantabria"
            "ES-CM" -> viewModel.ccaaName.value = "Castilla-La Mancha"
            "ES-CL" -> viewModel.ccaaName.value = "Castilla y León"
            "ES-CT" -> viewModel.ccaaName.value = "Cataluña"
            "ES-CE" -> viewModel.ccaaName.value = "Ceuta"
            "ES-VC" -> viewModel.ccaaName.value = "Comunidad Valenciana"
            "ES-EX" -> viewModel.ccaaName.value = "Extremadura"
            "ES-GA" -> viewModel.ccaaName.value = "Galicia"
            "ES-MD" -> viewModel.ccaaName.value = "Madrid"
            "ES-ML" -> viewModel.ccaaName.value = "Melilla"
            "ES-MC" -> viewModel.ccaaName.value = "Murcia"
            "ES-NC" -> viewModel.ccaaName.value = "Navarra"
            "ES-PV" -> viewModel.ccaaName.value = "Pais Vasco"
            "ES-RI" -> viewModel.ccaaName.value = "La Rioja"
        }
    }
}
package es.rudo.covidmap.modules.maps

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.covidmap.api.RetrofitClient
import es.rudo.covidmap.data.model.Header
import es.rudo.covidmap.data.model.Region
import es.rudo.covidmap.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class MapsViewModel : ViewModel() {
    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    val regionList = MutableLiveData<ArrayList<Region>>()
    val serverError = MutableLiveData<Boolean>()
    val setMap = MutableLiveData<Boolean>()

    fun getRegion(/*setMap: () -> Unit*/) {
        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getRegion()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseHeader: Header = response.body() as Header
                            regionList.value = responseHeader.timeline[0].regiones
                            setMap.value = true
                            /* setMap()*/
                        } else serverError.value = true
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}
package es.rudo.covidmap.helpers

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import es.rudo.covidmap.App
import es.rudo.covidmap.R
import java.text.SimpleDateFormat
import java.util.*

class DatePikerFragment(val listener: (day: Int, month: Int, year: Int) -> Unit) :
    DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        val month = Calendar.getInstance().get(Calendar.MONTH)
        val year = Calendar.getInstance().get(Calendar.YEAR)
        val cMin: Calendar = parseData(App.preferences.getMinDate())
        val cMax: Calendar = parseData(App.preferences.getMaxDate())
        val picker =
            DatePickerDialog(activity as Context, R.style.date_picker_style, this, year, month, day)
        picker.datePicker.maxDate = cMax.timeInMillis
        picker.datePicker.minDate = cMin.timeInMillis

        return picker
    }

    private fun parseData(date: String?): Calendar {
        val sdf = SimpleDateFormat(Constants.SERVER_DATE_PATTERN)
        val date = sdf.parse(date)
        val c: Calendar = Calendar.getInstance()
        c.time = date
        return c
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        listener(dayOfMonth, month, year)
    }
}
